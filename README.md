# Cisco APIC-EM API Library
## List of content

 - [About this repo](#about-this-repo)
 - [What is APIC-EM?](#what-is-apic-em)
	 - [What is it good for?](#what-is-it-good-for)
	 - [Can I try it?](#can-i-try-it)
- [About this library](#about-this-library)
	- [APIC_Core](#apic_core)
		- [Parameters](#apic_core-parameters)
	- [Models](#models)
		- [Filter](#filter)
			- [Parameters](#filter-parameters)

## About this repo
This repository aims to provide simple and efficient way to communicate with Cisco's APIC-EM via Rest API. This python library can be used mainly to retrieve various data from APIC-EM database, but can also handle some basic control of the APIC-EM.

## What is APIC-EM?
[APIC-EM](https://www.cisco.com/c/en/us/products/cloud-systems-management/application-policy-infrastructure-controller-enterprise-module/index.html), or Application Policy Infrastructure Controller Enterprise Module, is a freely available SDN controller developed by Cisco. It provides a layer of abstraction above standard network topology, by retrieving data from network devices (such as switches and routers) and presenting these data via various dashboards to the user. 

### What is it good for?
The basic installation of APIC-EM comes with few "apps", each handling a different problem. Apart from basic tasks, such as displaying the network topology, displaying device inventory etc., these apps can perform task to help System Engineers to better understand and operate their network. However, the main strength of this controller lies in the ability to retrieve data about devices and hosts in a machine-readable format via Rest API. These data can then be used to create various applications.
> **Note**: Do not confuse APIC-EM and APIC. APIC is the full-blown SDN controller for Cisco Nexus Switches for Datacenter enviroments. It has been renamed and is developed as **[ACI](https://www.cisco.com/c/en/us/solutions/data-center-virtualization/application-centric-infrastructure/index.html)** (Application Centric Infrastructure). APIC-EM is "less SDN" version of the ACI for enterprise networks.
### Can I try it?
Sure, the best place to start is [Cisco's DevNet](https://developer.cisco.com/site/apic-em/), where you get some basic understanding of APIC-EM together with some hands-on labs. You can also check the online sandbox version at [sandboxapic.cisco.com](sandboxapic.cisco.com) (Username: "devnetuser", Password: "Cisco123!")



# About the library

## APIC_Core
This library consists of multiple classes, each handling a different task. The most important class is the `APIC_Core`, which handles the "low-level" communication with APIC-EM's Rest API. It performs tasks such as:

 - Connecting to the APIC-EM (obviously)
 - Handling the ticket retrieval and renewal
 - Basic GET, POST and PUT methods
 - Checking the APIC-EM responses and handling them accordingly
 - And more ;-)

**Example:**
```python
# Create instance of APIC_Core()
apic = APIC_Core(
	APIC_HOST="sandboxapic.cisco.com",
	APIC_LOGIN="devnetuser",
	APIC_PASSWD="Cisco123!",
	DEBUG=False
	)
# Get count of all devices in APIC-EM's database
device_count = apic.get(path="/network-device/count")
# Print the result
print(f"Network device count is {device_count}")
```
### APIC_Core Parameters
**APIC_HOST** -
IP address or hostname of APIC-EM. Ex.: "sandboxapic.cisco.com". Do not enter `https://` in the beginning.

**APIC_LOGIN** -
Username for logging in and getting service ticket.

**APIC_PASSWD** -
Password for logging in and getting service ticket

**APIC_BASE** -
This represents the base path of the API URL. The default is:
```python
"https://" + APIC_HOST + "/api/v1"
```
If Cisco ever introduces v2 of the API, this should ensure future compatibility.

**CREDENTIALS** -
Path to JSON file containing credentials and service ticket. The default path is in the package folder data: `Data/credentials.json`
Sample credentials file:
```json
{"username": "devnetuser", "password": "Cisco123!", "url": "sandboxapic.cisco.com", "token": "ST-754-t5sZxpbYxWbWmor1W0O2-cas"}
```
**DEBUG** -
Boolean - Enables debug output. Default False.

**VERIFY** -
Boolean - Whether or not to check validity of SSL certificate. Default False.

**cache_credentials** -
Boolean - Whether or not to store credentials and service ticket (token) in `CREDENTIALS` file.

 ## Models
 Model classes represent certain parts of the APIC-EM database. The main goal of Models is to simplify operation with certain types of data. Each model has "model".dict variable, which holds all the retrieved information in a form of dictionary. For example, let's take `Devices` model:
This model handles various actions regarding devices inventory. It can retrieve data based on filter criteria (see [Filter](#filter) )

### Filter
#### Filter Parameters