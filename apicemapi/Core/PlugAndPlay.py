from apicemapi.Core.Models import Model
from apicemapi.Core.Base import APIC_Core
from apicemapi.utils.utils import get_logger
import json

class PlugAndPlay:
    """
    This class need a little rewrite, as it was written ad-hoc
    """
    def __init__(self, apic=None, DEBUG=False, lowleveldebug=False):
        self.apic = apic
        self.DEBUG = DEBUG
        self.logger = get_logger(name="PlugAndPlay", DEBUG=self.DEBUG)
        if isinstance(apic, APIC_Core):
            self.apic = apic
            self.logger.info(msg="Using given APIC: %s" % self.apic)
        else:
            self.apic = APIC_Core(DEBUG=lowleveldebug)
            self.logger.info(msg="Using default APIC: %s" % self.apic)


    def template_get(self, id=None, filename=None):
        file_id = None
        if isinstance(filename, str):
            file_id = self.apic.file_exists(namespace="template", file_path=filename)
        if isinstance(id, str):
            response = self.apic.get(path="/template/{0}".format(id))
        else:
            response = self.apic.get(path="/template")
            if file_id:
                for template in response:
                    if template["fileId"] == file_id:
                        return template

        return response

    def template_create(self, fileId=None, namespace=None, file_path=None):
        if fileId:
            # Received fileId, check if fileId exists
            response = self.apic.get(path="/file/{0}".format(fileId))
            if response:
                # FIle with given ID exists
                self.logger.debug(msg="PlugAndPlay : Get_Project : File with ID:'{0}' exists.".format(fileId))
        elif namespace and file_path:
            fileId = self.apic.file_exists(namespace=namespace, file_path=file_path)
            if fileId:
                self.logger.debug(msg="PlugAndPlay : Template Create : File {0} exists in namespace {1} with id {3}".format(file_path.split("/")[-1], namespace, fileId))
            else:
                fileId = self.apic.upload_file(namespace=namespace, file_path=file_path)
                template = self.apic.post(path="/template", data=json.dumps(
                    [
                        {
                            "fileId": fileId
                        }
                    ]
                ))
                print(template)


    def project_get(self, id=None, siteName=None):
        if id:
            projects = self.apic.get(path="/pnp-project/{0}".format(id))
            return projects
        else:
            projects = self.apic.get(path="/pnp-project")
            self.logger.debug(msg="PlugAndPlay : Get_Project : Found {0} projects.".format(len(projects)))
            if siteName:
                for project in list(projects):
                    if project["siteName"] != siteName:
                        projects.remove(project)
            if len(projects) == 0:
                projects = None
                self.logger.debug(msg="PlugAndPlay : Get_Project : Project with siteName '{0}' does not exist.".format(siteName))
            elif len(projects) == 1:
                projects = projects[0]
                self.logger.debug(msg="PlugAndPlay : Get_Project : Found 1 project with siteName '{0}'".format(siteName))
            return projects

    def project_create(self, siteName, tftpServer=None, tftpPath=None, note=None):
        if not isinstance(siteName, str):
            self.logger.error(msg="PlugAndPlay : Create_Project : Missing required argument 'siteName'. Returning: None.")
            return None
        # Check if siteName already exists
        project = self.project_get(siteName=siteName)
        if project:
            self.logger.info(msg="PlugAndPlay : Create_Project : Project '{0}' already exists with ID: '{1}'".format(siteName, project["id"]))
            return project["id"]

        body = [{
            "siteName": siteName
        }]
        if tftpServer:
            body[0]["tftpServer"] = tftpServer
        if tftpPath:
            body[0]["tftpPath"] = tftpPath
        if note:
            body[0]["note"] = note
        # Convert body to JSON string
        body = json.dumps(body)

        taskId = None
        response = self.apic.post("/pnp-project", body)
        if isinstance(response, dict):
            taskId = response["taskId"]
            self.logger.debug(msg="PlugAndPlay : Create_Project : Server returned taskId '{0}'".format(taskId))
            projectId = self.apic.task_handler(taskId=taskId)
            return projectId
        else:
            self.logger.error(msg="PlugAndPlay : Create_Project : Server did not return valid response.")
            return None

    def project_delete(self, siteName=None, id=None):
        project = None
        if id:
            project = self.project_get(id=id)
        elif siteName:
            project = self.project_get(siteName=siteName)
        if not project:
            self.logger.warning(msg="PlugAndPlay : Delete_Project : Specified Project does not exist.")
            return True
        else:
            if not id:
                id = project["id"]
            if not siteName:
                siteName = project["siteName"]
        # Delete existing project
        response = self.apic.delete(path="/pnp-project/{0}".format(id))
        if response:
            taskId = response["taskId"]
            result = self.apic.task_handler(taskId=taskId)
            if result:
                self.logger.info(msg="PlugAndPlay : Delete_Project : Successfully deleted Project '{0}' (ID: '{1}')".format(siteName, id))
                return result
            else:
                self.logger.error(msg="PlugAndPlay : Delete_Project : Failed to delete Project '{0}' (ID: '{1}')".format(siteName, id))
                return None

    def template_config_create(self, configProperty, template_id=None, template_name=None):
        if not template_id and template_name:
            template_id = self.template_get(filename=template_name)["id"]
            print(template_id)
        body = [
          {
            "name": template_name.split(".")[0],
            "generate": False,
            "templateId": template_id,
            "configProperty": configProperty,
          }
        ]
        # Create Config
        response = self.apic.post(path="/template-config", data=json.dumps(body))
        taskId = response["taskId"]
        template_config_id = self.apic.task_handler(taskId=taskId)
        return template_config_id

    def image_get(self, image_name):
        image_id = None
        response = self.apic.get(path=f"/pnp-file/image")
        if response:
            for image in response["images"]:
                if image["imageName"] == image_name:
                    image_id = image["imageId"]
        return image_id

    def device_create(self, device_json, project_id=None, project_name=None):
        if project_id:
            pass
        elif project_name:
            projects = self.apic.get(path=f"/pnp-project")
            for project in projects:
                if project["siteName"] == project_name:
                    project_id = project["id"]
                    break
        response = self.apic.post(path=f"/pnp-project/{project_id}/device", data=device_json)
        if not response:
            self.logger.error(f"Failed to create device {json.loads(device_json)[0]['hostName']}")
            return None
        task_id = response["taskId"]
        result = self.apic.task_handler(taskId=task_id)
        return result




def master_loop():
    results = []
    devices = []
    lines = []
    with open(r"C:\Users\Mira\CloudStation\Work\ALEF\Penny\devices_01.csv", mode="r") as f:
        for line in f:
            line = line.strip("\n")
            line_list = line.split(",")
            if len(line_list) > 0:
                lines.append(line_list)
    headers = lines[0]
    lines = lines[1:]
    for line in lines:
        device = {}
        for i in range(len(headers)):
            device[headers[i]] = line[i]
        devices.append(device)
    print(f"Loaded {len(devices)} devices.")

    PROJECT_NAME = "Prodejny-Radonice-01"
    PNP_CLIENT = PlugAndPlay(DEBUG=True, lowleveldebug=False)
    PROJECT_ID = PNP_CLIENT.project_get(siteName=PROJECT_NAME)["id"]
    project_devices = PNP_CLIENT.apic.get(path=f"/pnp-project/{PROJECT_ID}/device")
    project_devices_hostnames = [x["hostName"] for x in project_devices]
    print(f"Project {PROJECT_NAME} ID is: {PROJECT_ID}")
    print(f"Project {PROJECT_NAME} contains {len(project_devices_hostnames)} devices.")

    template_map = {}
    image_map = {}
    device_body = {
        "serialNumber": "",
        "platformId": "",
        "imageId": "",
        "pkiEnabled": False,
        "hostName": "",
        "sudiRequired": False,
        "deviceDiscoveryInfo": {
            "userNameList": [""],
            "passwordList": [""],
            "reDiscovery": False,
            "enablePasswordList": [
                ""
            ],
        },
        "eulaAccepted": True,
        "templateConfigId": "",
      }
    for device in devices[1:]:
        if device["hostname"] in project_devices_hostnames:
            print(f"Device '{device['hostname']}' is already present in project.")
            continue
        template_id = None
        if device["Template"] in template_map.keys():
            template_id = template_map[device["Template"]]
        else:
            template_id = PNP_CLIENT.template_get(filename=device["Template"])["id"]
            template_map[device["Template"]] = template_id
            print(f"Template ID for template '{device['Template']}' is: {template_id} ")
        image_id = None
        if device["Image"] in image_map.keys():
            image_id = image_map[device["Image"]]
        else:
            image_id = PNP_CLIENT.image_get(image_name=device["Image"])
            image_map[device["Image"]] = image_id
            print(f"Image ID for image '{device['Image']}' is: {image_id} ")
        configProperty = {
            "ip_address": device["ip_address"],
            "hostname": device["hostname"],
            "gateway": device["gateway"]
        }

        ## Generate Template Config
        template_config_id = PNP_CLIENT.template_config_create(configProperty=configProperty,
                                                               template_id=template_id,
                                                               template_name=f"{device['Template']}")
        print(f"Template Config ID for device {device['hostname']} is: {template_config_id}")

        if len(device["Serial Number"]) != 11:
            print(f"Invalid SN for device {device['hostname']}")
            if "serialNumber" in device_body.keys():
                del device_body["serialNumber"]
                print(list(device_body.keys()))
        else:
            device_body["serialNumber"] = device["Serial Number"]
        device_body["hostName"] = device["hostname"]
        device_body["platformId"] = device["Product ID"]
        device_body["imageId"] = image_id
        device_body["templateConfigId"] = template_config_id
        device_body["deviceDiscoveryInfo"]["userNameList"] = [device["Username"]]
        device_body["deviceDiscoveryInfo"]["passwordList"] = [device["Password"]]

        # Create Device
        result = PNP_CLIENT.device_create(project_id=PROJECT_ID, device_json=json.dumps([device_body]))
        if result:
            print(f"Success: Device {device['hostname']}")
        else:
            print(f"FAILURE: Device {device['hostname']}")


if __name__ == '__main__':
    master_loop()

    #pnp_client = PlugAndPlay(DEBUG=True, lowleveldebug=True)
    #print(pnp_client.apic.get(path="/pnp-project/5623af53-ba1e-4b49-a5a2-d319ed62d9bf/device"))

