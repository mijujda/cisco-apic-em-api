from apicemapi.Core.Base import APIC_Core
import logging
import json
import time

class CommandRunner:

    def __init__(self, request, apic=None, DEBUG=False, lowleveldebug=False):
        self.DEBUG = DEBUG
        self.logger = logging.getLogger("CommandRunner")
        if self.DEBUG:
            self.logger.setLevel(level=logging.DEBUG)
        else:
            self.logger.setLevel(level=logging.ERROR)

        self.dict = request
        self.apic = None
        if apic is None:
            self.apic = APIC_Core(DEBUG=lowleveldebug)
        elif isinstance(apic, APIC_Core):
            self.apic = apic
        else:
            self.logger.error(msg="Argument apic is not a valid instance of APIC Core class.")

        self.runner()

    def send_request(self, body):
        response = self.apic.post(path="/network-device-poller/cli/read-request", data=body)
        return response

    def craft_request(self, device_id):
        """
        Generate request body for given tasks
        """
        self.dict[device_id]["requests"] = []

        task_distinguisher = 0
        while len(self.dict[device_id]["commands"]) > 5:
            self.dict[device_id]["requests"].append(json.dumps({
                "name": self.dict[device_id]["name"] + " " + str(task_distinguisher),
                "description": self.dict[device_id]["description"] + " " + str(task_distinguisher),
                "timeout": 0,
                "commands": self.dict[device_id]["commands"][0:5],
                "deviceUuids": [
                    self.dict[device_id]["deviceId"]
                ]
            }))
            task_distinguisher += 1
            self.dict[device_id]["commands"] = self.dict[device_id]["commands"][5:len(self.dict[device_id]["commands"])]
        if len(self.dict[device_id]["commands"]) > 0:
            self.dict[device_id]["requests"].append(json.dumps({
                "name": self.dict[device_id]["name"] + " " + str(task_distinguisher),
                "description": self.dict[device_id]["description"] + " " + str(task_distinguisher),
                "timeout": 0,
                "commands": self.dict[device_id]["commands"],
                "deviceUuids": [
                    self.dict[device_id]["deviceId"]
                ]
            }))
        del self.dict[device_id]["commands"]
        self.logger.debug(msg="Crafted %d for device %s" % (len(self.dict[device_id]["requests"]), device_id))

    def runner(self):
        for device_id, device in list(self.dict.items()):
            device["taskIds"] = []
            device["fileIds"] = []
            device["response"] = {}
            # Craft requests
            self.craft_request(device_id=device_id)
            self.logger.debug(msg="Request for device %s crafted." % device_id)
        i = 0
        devices = list(self.dict.keys())
        while i < len(self.dict):
            for device_id, device in list(self.dict.items()):
                if device_id in devices and len(device["requests"]) == 0:
                    i += 1
                    devices.remove(device_id)
                    self.logger.debug(msg="All requests for device %s have been sent." % device_id)
                    continue

                # Get taskIds
                while len(list(device["requests"])) > 0:
                    try:
                        response = self.apic.raw_post(path="/network-device-poller/cli/read-request", data=device["requests"][0])
                        time.sleep(0.1)
                        if response.status_code == 202:
                            device["taskIds"].append(response.json()["response"]["taskId"])
                            self.logger.debug(msg="Request sent, TaskId: %s" % device["taskIds"][-1])
                            # Remove request that received Task ID
                            device["requests"] = device["requests"][1:len(device["requests"])]
                            # TODO: Make it faster
                            # Skip to next device
                            break
                        elif response.status_code == 400:
                            self.logger.debug(msg="Not ready to accept new request yet...")
                            # Skip to next device
                            break
                        else:
                            self.logger.error(msg="Something went wrong, unhandled state.")
                    except Exception as e:
                        self.logger.error(msg="Failed to get Task ID for device %s, request: '%s'. Exception states: %s" % (device_id, device["requests"][0], repr(e)))
                        time.sleep(1)
                        break

        # All requests have been sent #

        # Getting File ID
        i = 0
        # Loop until all devices have finished
        devices = list(self.dict.keys())
        while i < len(self.dict):
            # Loop over devices
            for device_id, device in list(self.dict.items()):
                # Loop until all tasks are finished
                while len(device["taskIds"]) > 0:
                    # Loop over tasks
                    for taskId in device["taskIds"]:
                        """
                        THIS SECTION: Loops over all TaskIDs. If for certain Task ID a valid FileID is found, the FileId 
                        is kept and the Task ID is removed. This way, during the next cycle of higher level loop, only 
                        Task IDs without results are used.
                        """
                        response = self.apic.get(path="/task/%s" % taskId)["progress"]
                        self.logger.debug(msg="Checking task progress for TaskID: %s" % taskId)
                        if "fileId" in response:
                            # Task has finished
                            fileId = json.loads(response)["fileId"]
                            device["fileIds"].append(fileId)
                            file_content = self.apic.raw_get(path="/file/%s" % fileId).json()[0]["commandResponses"]["SUCCESS"]
                            device["response"].update(file_content)
                            self.logger.debug(msg="Task %s finished, result in file %s writen." % (taskId, fileId))
                            device["taskIds"].remove(taskId)
                        else:
                            #Task has not finished yet, continue to next task
                            continue
                        time.sleep(0.3)
                    if device_id in devices and len(device["taskIds"]) == 0:
                        self.logger.debug(msg="All tasks for device %s have finished." % device_id)
                        # This device is complete
                        i += 1
                        devices.remove(device_id)

                    time.sleep(0.3)
        return self.dict
