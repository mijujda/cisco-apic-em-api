from apicemapi.Core.Base import APIC_Core
from apicemapi.Core.CommandRunner import CommandRunner
from apicemapi.RunParse.Parsers import L2_Trunk, VlanBrief
from apicemapi.utils.utils import get_logger
import logging
import json
import pkg_resources


class Filter:
    """
    Object for making filtering of required field easier
    """
    def __init__(self, required={}, excluded={}, exact_match=True):
        self.logger = get_logger(name="Filter")
        self.required = None
        self.excluded = None
        if isinstance(required, dict):
            self.required = required
        else:
            self.required = {}
            self.logger.error(msg="Required is not a dictionary!")
        if isinstance(excluded, dict):
            self.excluded = excluded
        else:
            self.excluded = {}
            self.logger.error(msg="Excluded is not a dictionary!")
        self.exact_match = exact_match

    def __str__(self):
        return "[FilterObject: Required='%s' Excluded='%s' Exact-match: %s]" % (str(self.required), str(self.excluded), str(self.exact_match))


class OutputFilter:
    """
    Object for making filtering dictionaries easier
    """
    def __init__(self, data=None, required=[], excluded=[]):
        self.logger = logging.getLogger("OutputFilter")
        self.logger.setLevel(level=logging.ERROR)
        self.data = data
        self.required = None
        self.excluded = None
        if isinstance(required, list):
            self.required = required
        else:
            self.required = []
            self.logger.error(msg="Required is not a list!")
        if isinstance(excluded, list):
            self.excluded = excluded
        else:
            self.excluded = []
            self.logger.error(msg="Excluded is not a list!")

    def get(self):
        if isinstance(self.data, dict):
            for id, data in list(self.data.items()):
                if len(self.required) > 0:
                    # Loop over required keys
                    for key in list(data.keys()):
                        if key not in self.required:
                            del self.data[id][key]
                elif len(self.excluded) > 0:
                    # Loop over excluded keys
                    for key in list(data.keys()):
                        if key in self.excluded:
                            del self.data[id][key]
                else:
                    continue
            return self.data

        elif isinstance(self.data, list):
            for i in range(len(self.data)):
                if isinstance(self.data[i], dict):
                    if len(self.required) > 0:
                        for key in list(self.data[i].keys()):
                            if key not in self.required:
                                del self.data[i][key]
                    elif len(self.blocked) > 0:
                        for key in list(self.data[i].keys()):
                            if key in self.excluded:
                                del self.data[i][key]
                    else:
                        continue
            return self.data

        else:
            self.logger.error(msg="Data is not a dict or list!")
            return None


class Model(object):
    """
    Base class for models
    """
    def __init__(self, apic=None, type=None, filter=None, passive=False, DEBUG=False, lowleveldebug=False):
        self.DEBUG = DEBUG
        self.passive = passive
        self.lowleveldebug = lowleveldebug
        self.type = None
        types = ["Interface", "Device", "Topology", "PathTrace", "Module", "PlugAndPlay"]
        if type in types:
            self.type = type
        self.datafile_path = pkg_resources.resource_filename("apicemapi", "Data/%s.json" % self.type)
        self.logger = get_logger(name=type, DEBUG=self.DEBUG)
        """
        if self.DEBUG:
            self.logger.setLevel(level=logging.DEBUG)
        else:
            self.logger.setLevel(level=logging.INFO)
        """
        self.apic = None
        if isinstance(apic, APIC_Core):
            self.apic = apic
            self.logger.info(msg="Using given APIC: %s" % self.apic)
        else:
            self.apic = APIC_Core(DEBUG=lowleveldebug)
            self.logger.info(msg="Using default APIC: %s" % self.apic)

        self.dict = {}
        self.filter = None
        if isinstance(filter, Filter):
            self.filter = filter
            self.logger.debug(msg="Using filter: %s" % str(self.filter))
        elif filter is None:
            self.logger.debug(msg="Not using any filter")
        else:
            self.logger.error(msg="Received filter is not an instance of Filter.")

        if self.passive:
            try:
                self.load_data()
            except Exception as e:
                self.logger.warning(msg="Failed to load data from file while using passive mode.")

    def populate(self, response):
        """
        This function writes given data to the self.dict
        :param response: (List) Data returned by APIC_Core
        :return:
        """
        if isinstance(response, dict):
            try:
                self.dict[response["id"]] = response
            except Exception as e:
                self.logger.error(msg="Populate: Exception: %s" % e)
        elif isinstance(response, list):
            for element in response:
                try:
                    self.dict[element["id"]] = element
                except Exception as e:
                    self.logger.error(msg="Populate: Exception: %s" % e)
        elif response is None:
            self.logger.error(msg="Populate: Received None response")
        else:
            self.logger.error(msg="Populate: Something bad happened")

    def dict_cleanup(self, data, filter):
        if filter is None:
            self.logger.debug(msg="No filter given to cleanup.")
            return data
        elif not isinstance(filter, Filter):
            self.logger.error(msg="Dict-Cleanup: Given filter is not a valid instance of Filter.")
            return data

        for data_key, data_value in list(data.items()):
            # TODO: Add DEBUG logging (?)
            for filter_key, filter_value in filter.required.items():
                if filter_key in data_value.keys():
                    if isinstance(filter_value, str) and filter.exact_match:
                        if data_value[filter_key] != filter_value:
                            del data[data_key]
                            break
                    elif isinstance(filter_value, str) and (not filter.exact_match):
                        if data_value[filter_key] is None:
                            del data[data_key]
                            break
                        if filter_value not in data_value[filter_key]:
                            del data[data_key]
                            break
                    elif isinstance(filter_value, list) and filter.exact_match:
                        if data_value[filter_key] not in filter_value:
                            del data[data_key]
                            break
                    elif isinstance(filter_value, list) and (not filter.exact_match):
                        if data_value[filter_key] is None:
                            del data[data_key]
                            break
                        found_match = False
                        for filter_value_item in filter_value:
                            if filter_value_item in data_value[filter_key]:
                                found_match = True
                        if not found_match:
                            del data[data_key]
                            break
                    else:
                        self.logger.warning(msg="Dict_Cleanup: None of the cases matched. Data: %s Filter: %s" % (data_value, self.filter))
                        # TODO: Handle other possible cases
                else:
                    self.logger.warning(msg="Dict_Cleanup: Filter key: %s not present in Data: %s" % (filter_key, data_value))
                    continue

        for data_key, data_value in list(data.items()):
            # TODO: Add DEBUG logging (?)
            for filter_key, filter_value in filter.excluded.items():
                if filter_key in data_value.keys():
                    if isinstance(filter_value, str) and filter.exact_match:
                        if data_value[filter_key] == filter_value:
                            del data[data_key]
                            break
                    elif isinstance(filter_value, str) and (not filter.exact_match):
                        if data_value[filter_key] is None:
                            continue
                        if filter_value in data_value[filter_key]:
                            del data[data_key]
                            break
                    elif isinstance(filter_value, list) and filter.exact_match:
                        if data_value[filter_key] in filter_value:
                            del data[data_key]
                            break
                    elif isinstance(filter_value, list) and (not filter.exact_match):
                        if data_value[filter_key] is None:
                            continue
                        found_match = False
                        for filter_value_item in filter_value:
                            if filter_value_item in data_value[filter_key]:
                                found_match = True
                        if found_match:
                            del data[data_key]
                            break
                    else:
                        self.logger.warning(msg="Dict_Cleanup: None of the cases matched. Data: %s Filter: %s" % (data_value, self.filter))
                        # TODO: Handle other possible cases
                else:
                    self.logger.warning(msg="Dict_Cleanup: Filter key: %s not present in Data: %s" % (filter_key, data_value))
                    continue
        return data

    def list_cleanup(self, data, filter):
        if filter is None:
            self.logger.debug(msg="No filter given to cleanup.")
            return data
        elif not isinstance(filter, Filter):
            self.logger.error(msg="List_Cleanup: Given filter is not a valid instance of Filter.")
            return None

        for data_value in list(data):
            # TODO: Add DEBUG logging (?)
            for filter_key, filter_value in filter.required.items():
                if filter_key in data_value.keys():
                    if isinstance(filter_value, str) and filter.exact_match:
                        if data_value[filter_key] != filter_value:
                            data.remove(data_value)
                            break
                    elif isinstance(filter_value, str) and (not filter.exact_match):
                        if data_value[filter_key] is None:
                            data.remove(data_value)
                            break
                        if filter_value not in data_value[filter_key]:
                            data.remove(data_value)
                            break
                    elif isinstance(filter_value, list) and filter.exact_match:
                        if data_value[filter_key] not in filter_value:
                            data.remove(data_value)
                            break
                    elif isinstance(filter_value, list) and (not filter.exact_match):
                        if data_value[filter_key] is None:
                            data.remove(data_value)
                            break
                        found_match = False
                        for filter_value_item in filter_value:
                            if filter_value_item in data_value[filter_key]:
                                found_match = True
                        if not found_match:
                            data.remove(data_value)
                            break
                    else:
                        self.logger.warning(msg="List_Cleanup: None of the cases matched. Data: %s Filter: %s" % (data_value, self.filter))
                        # TODO: Handle other possible cases
                else:
                    self.logger.warning(msg="List_Cleanup: Filter key: %s not present in Data: %s" % (filter_key, data_value))
                    continue

        for data_value in list(data):
            # TODO: Add DEBUG logging (?)
            for filter_key, filter_value in filter.excluded.items():
                if filter_key in data_value.keys():
                    if isinstance(filter_value, str) and filter.exact_match:
                        if data_value[filter_key] == filter_value:
                            data.remove(data_value)
                            break
                    elif isinstance(filter_value, str) and (not filter.exact_match):
                        if data_value[filter_key] is None:
                            continue
                        if filter_value in data_value[filter_key]:
                            data.remove(data_value)
                            break
                    elif isinstance(filter_value, list) and filter.exact_match:
                        if data_value[filter_key] in filter_value:
                            data.remove(data_value)
                            break
                    elif isinstance(filter_value, list) and (not filter.exact_match):
                        if data_value[filter_key] is None:
                            continue
                        found_match = False
                        for filter_value_item in filter_value:
                            if filter_value_item in data_value[filter_key]:
                                found_match = True
                        if found_match:
                            data.remove(data_value)
                            break
                    else:
                        self.logger.warning(msg="List_Cleanup: None of the cases matched. Data: %s Filter: %s" % (data_value, self.filter))
                        # TODO: Handle other possible cases
                else:
                    self.logger.warning(msg="List_Cleanup: Filter key: %s not present in Data: %s" % (filter_key, data_value))
                    continue

        return data

    def universal_cleanup(self, data=None, filter=None):
        if data is None:
            data = self.dict
        if filter is None:
            filter = self.filter

        output = None
        if isinstance(data, list):
            output = self.list_cleanup(data=data, filter=filter)
        elif isinstance(data, dict):
            output = self.dict_cleanup(data=data, filter=filter)
        else:
            self.logger.error(msg="Universal_Cleanup: Given data is neither list nor dict.")
        return output

    def write_data(self, path=None):
        if path is None:
            path = self.datafile_path
        with open(file=path, mode="w") as file:
            json.dump(obj=self.dict, fp=file, indent=2)
            self.logger.info(msg="Data written to datafile.")

    def load_data(self, path=None):
        if path is None:
            path = self.datafile_path
        with open(file=path, mode="r") as file:
            self.dict = json.load(fp=file)
            self.logger.info(msg="Data loaded from Datafile.")


    def __str__(self):
        return "[%s Object: %d]" % (self.type, len(self.dict))


class Interfaces(Model):
    # TODO: Handle returning more than 500 interfaces
    def __init__(self, apic=None, filter=None, passive=False, DEBUG=False, lowleveldebug=False):
        super(Interfaces, self).__init__(type="Interface", apic=apic, filter=filter, passive=passive, DEBUG=DEBUG, lowleveldebug=lowleveldebug)
        """
        :param apic: APIC_Core() instance which hadles the communication with APIC-EM
        :param filter: Instance of Filter class, which holds fields for filtering
        :param DEBUG: Boolean parameter which enables logging output
        """
        self.map = None
        if not passive:
            self.update()

    def update(self):
        self.path_finder()
        self.runner()
        self.dict = self.universal_cleanup()
        self.number_vlans()

    def path_finder(self):
        try:
            if "id" in self.filter.required.keys():
                self.map = {"path": "/interface/%s" % self.filter.required["id"], "params": None}
                # TODO: Add debug logging line
            elif "deviceId" in self.filter.required.keys() and "portName" in self.filter.required.keys():
                self.map = {"path": "/interface/network-device/%s/interface-name" % self.filter.required["deviceId"], "params": {"name": self.filter.required["portName"]}}
                # TODO: Add debug logging line
            elif "deviceId" in self.filter.required.keys():
                self.map = {"path": "/interface/network-device/%s" % self.filter.required["deviceId"], "params": None}
                # TODO: Add debug logging line
            elif "ipv4Address" in self.filter.required.keys():
                self.map = {"path": "/interface/ip-address/%s" % self.filter.required["ipv4Address"], "params": None}
                # TODO: Add debug logging line
            else:
                self.map = None
        except Exception as e:
            self.map = None
            self.logger.error(msg="PathFinder exception states: %s" % repr(e))

    def runner(self):
        if self.map is not None:
            response = self.apic.get(path=self.map["path"], params=self.map["params"])
        else:
            interface_count = int(self.apic.get(path="/interface/count"))
            if interface_count > 500:
                # TODO: Implement mechanism to retrieve all interfaces
                self.logger.warning(msg="Trying to get %d interfaces! Maximum supported by APIC is 500." % interface_count)
            response = self.apic.get(path="/interface")
        self.populate(response)

    def get_trunk_vlans(self):
        sh_command = "show running-config interface "
        data = {}
        for int_id, interface in list(self.dict.items()):
            if interface["portMode"] == "trunk":

                if interface["deviceId"] not in data.keys():
                    data[interface["deviceId"]] = {"deviceId": interface["deviceId"],
                                                   "commands": [],
                                                   "description": "%s trunk VLANs" % interface["deviceId"],
                                                   "name": "%s trunk VLANs" % interface["deviceId"]
                                                   }


                data[interface["deviceId"]]["commands"].append(sh_command + interface["portName"])
        response = CommandRunner(request=data, DEBUG=self.DEBUG, lowleveldebug=False).dict
        for int_id, interface in list(self.dict.items()):
            if interface["portMode"] == "trunk" and interface["deviceId"] in response.keys():
                output = L2_Trunk(response[interface["deviceId"]]["response"][sh_command + interface["portName"]].split("\n")).dict
                interface["vlanId"] = output["allowedVlans"]
                interface["nativeVlanId"] = output["nativeVlan"]

    def number_vlans(self):
        for int_id, interface in self.dict.items():
            try:
                interface["vlanId"] = [int(interface["vlanId"])]
            except:
                pass

class Devices(Model):

    def __init__(self, apic=None, filter=None, passive=False, DEBUG=False, lowleveldebug=False):
        super(Devices, self).__init__(type="Device", apic=apic, filter=filter, passive=passive, DEBUG=DEBUG, lowleveldebug=lowleveldebug)

        self.map = None
        self.devices_file_path = pkg_resources.resource_filename("apicemapi", "Data/devices.json")
        if not passive:
            self.update()

    def update(self):
        self.path_finder()
        self.runner()
        self.dict = self.universal_cleanup()

    def path_finder(self):
        if isinstance(self.filter, Filter):
            if "id" in self.filter.required.keys():
                self.map ={"path": "/network-device/%s" % self.filter.required["id"], "params": None}
                self.logger.debug(msg="Map-Path: '%s' Map-Params: '%s'" % (self.map["path"], self.map["params"]))
            else:
                self.map = None
        else:
            self.map = None



    def runner(self):
        if self.map is not None:
            response = self.apic.get(path=self.map["path"], params=self.map["params"])
        else:
            response = self.apic.get(path="/network-device")
        self.populate(response)

    def get_device_int(self, filter=None):

        for id, device in list(self.dict.items()):

            if isinstance(filter, Filter):
                filter.required["deviceId"] = device["id"]
            else:
                filter = Filter(required={"deviceId": device["id"]})

            interfaces = Interfaces(apic=self.apic, filter=filter, DEBUG=self.lowleveldebug).dict
            if len(interfaces) > 0:
                self.logger.debug(msg="Got %d interfaces for device %s" % (len(interfaces), device["hostname"]))
            else:
                self.logger.debug(msg="Got no interfaces for device %s" % device["hostname"])
            device["interfaces"] = interfaces

    def get_vlans(self):
        sh_command = "show vlan brief"
        data = {}
        for dev_id, device in list(self.dict.items()):
            if device["family"] == "Switches and Hubs":
                data[dev_id] = {
                    "deviceId": dev_id,
                     "commands": [sh_command],
                     "description": "%s VLAN Brief" % device["hostname"],
                     "name": "%s VLAN Brief" % device["hostname"]
                     }
            else:
                # TODO: Implement VLANs for Routers
                pass
        response = CommandRunner(apic=self.apic, request=data, DEBUG=self.DEBUG, lowleveldebug=False).dict

        for dev_id in response.keys():
            parsed = VlanBrief(response[dev_id]["response"][sh_command]).dict
            self.dict[dev_id]["vlans"] = parsed

    def get_trunk_vlans(self, passive=False):
        """
        This method fetches allowed VLANs and Native VLAN for Trunk ports
        If passive=False => The data is retrieved by running commands per device via CommandRunner
        If passive=True => The data is retrieved from self.dict based on device vlans and ports (need to run self.get_vlans() first)
        :param passive:
        :return:
        """
        if passive:
            for dev_id, device in self.dict.items():
                if "vlans" not in device.keys() and device["family"] in ["Switches and Hubs", "Routers"]:
                    self.logger.debug("Get_Trunk_Vlans: Could not find any VLANs for device %s.")
                elif "vlans" in device.keys():
                    for int_id, interface in self.dict[dev_id]["interfaces"].items():
                        if interface["portMode"] == "trunk":
                            interface["vlanId"] = []
                            try:
                                interface["nativeVlanId"] = int(interface["nativeVlanId"])
                            except:
                                pass
                            for vlan_id, vlan in self.dict[dev_id]["vlans"].items():
                                if interface["portName"] in vlan["ports"]:
                                    print(".")
                                    interface["vlanId"].append(vlan_id)
                                else:
                                    print(interface["portName"], " not in ", vlan["ports"])
                                    print("!")
                else:
                    # This happens for devices other than switches and routers
                    pass

        else:
            sh_command = "show running-config interface "
            data = {}
            device_added = False
            for dev_id, device in list(self.dict.items()):
                if "interfaces" in device.keys():
                    for int_id, interface in list(device["interfaces"].items()):
                        if interface["portMode"] == "trunk":
                            if device_added:
                                pass
                            else:
                                data[dev_id] = {"deviceId": dev_id,
                                             "commands": [],
                                             "description": "%s trunk VLANs" % device["hostname"],
                                             "name": "%s trunk VLANs" % device["hostname"]
                                             }
                                device_added = True
                            data[dev_id]["commands"].append(sh_command + interface["portName"])
                device_added = False
            # Run CommandRunner
            response = CommandRunner(apic=self.apic, request=data, DEBUG=self.DEBUG, lowleveldebug=False).dict

            for dev_id in list(response.keys()):
                for int_id, interface in list(self.dict[dev_id]["interfaces"].items()):
                    if sh_command + interface["portName"] in response[dev_id]["response"].keys():
                        output = L2_Trunk(response[dev_id]["response"][sh_command + interface["portName"]].split("\n")).dict
                        # self.logger.debug(msg="Allowed VLANs for interface %s/%s: %s" % (self.dict[dev_id]["hostname"], interface["portName"], output))
                        interface["vlanId"] = output["allowedVlans"]
                        interface["nativeVlanId"] = output["nativeVlan"]


class Topology(Model):

    # TODO: Write Topology class
    def __init__(self, apic=None, filter=None, passive=False, DEBUG=False, lowleveldebug=False):
        super(Topology, self).__init__(type="Topology", apic=apic, filter=filter, passive=passive, DEBUG=DEBUG, lowleveldebug=lowleveldebug)
        self.topology_file_path = pkg_resources.resource_filename("apicemapi", "Data/topology.json")

    def get_pyhs_topo(self, node_filter=None):
        # TODO: Implement Nodes filter and Links filter (?)
        if node_filter is None:
            node_filter = self.filter
        response = self.apic.get(path="/topology/physical-topology")
        if response is not None:
            self.dict = response
            self.logger.debug(msg="Get_Phys_Topo: Got topology with %d nodes and %d links." % (len(self.dict["nodes"]), len(self.dict["links"])))
            if node_filter is not None:
                self.dict["nodes"] = self.universal_cleanup(data=self.dict["nodes"], filter=node_filter)
                self.link_cleanup()
                self.logger.debug(msg="Get_Phys_Topo: Got topology with %d nodes and %d links." % (len(self.dict["nodes"]), len(self.dict["links"])))
        else:
            self.logger.error(msg="Get_Phys_Topo: Failed to get response.")

    def get_l2_topo(self, vlan_id=None):
        # TODO: Get actual data from APIC
        devices = Devices(passive=True).dict
        interfaces = {}
        for dev_id, device in devices.items():
            if "interfaces" in device.keys():
                interfaces.update(device["interfaces"])

        for link in self.dict["links"]:
            if "startPortID" in link.keys():
                if link["startPortID"] in interfaces.keys():
                    link["sourceVlans"] = interfaces[link["startPortID"]]["vlanId"]
                    link["sourceNativeVlan"] = interfaces[link["startPortID"]]["nativeVlanId"]
                    link["sourcePortName"] = "%s/%s" % (
                        devices[interfaces[link["startPortID"]]["deviceId"]]["hostname"],
                        interfaces[link["startPortID"]]["portName"]
                    )
                else:
                    self.logger.warning(msg="No data found for interface %s" % (link["startPortID"]))
            if "endPortID" in link.keys():
                if link["endPortID"] in interfaces.keys():
                    link["targetVlans"] = interfaces[link["endPortID"]]["vlanId"]
                    link["targetNativeVlan"] = interfaces[link["endPortID"]]["nativeVlanId"]
                    link["targetPortName"] = "%s/%s" % (
                        devices[interfaces[link["endPortID"]]["deviceId"]]["hostname"],
                        interfaces[link["endPortID"]]["portName"]
                    )
                else:
                    self.logger.warning(msg="No data found for interface %s" % (link["endPortID"]))

        # TODO: Remove links and nodes that don't match VLAN ID
        #self.dict = self.topo_vlan_filter(vlan_id=vlan_id)
        # TODO: Finish L2 Topology

    def topo_vlan_filter(self, vlan_id, topo=None):
        if topo is None:
            topo = self.dict
        # Filter links
        for link in list(topo["links"]):
            if "sourceVlans" in link.keys() and link["sourceVlans"] is not None:
                if vlan_id in link["sourceVlans"]:
                    self.logger.debug(msg="Topo_Vlan_Filter: VLAN %d is present on source: %s" % (vlan_id, link["sourcePortName"]))
                    continue
            if "targetVlans" in link.keys() and link["targetVlans"] is not None:
                if vlan_id in link["targetVlans"]:
                    self.logger.debug(msg="Topo_Vlan_Filter: VLAN %d is present on target: %s" % (vlan_id, link["targetPortName"]))
                    continue
            topo["links"].remove(link)
            if "startPortID" in link.keys() and "endPortID" in link.keys():
                try:
                    self.logger.debug(msg="Topo_Vlan_Filter: VLAN %d is NOT present on link: %s->%s" % (vlan_id, link["sourcePortName"], link["targetPortName"]))
                except Exception as e:
                    pass
        return topo





    def trunk_check(self, topology=None):
        if topology is None:
            topology = self.dict
        misconfigured = {}
        for link in topology["links"]:
            if "sourceVlans" in link.keys() and "targetVlans" in link.keys():
                for vlan in link["sourceVlans"]:
                    if vlan not in link["targetVlans"]:
                        print("VLAN %d configured on source port (%s), but not on target port (%s)" % (vlan, link["startPortID"], link["endPortID"]))
                        if "%s/%s" % (link["startPortID"], link["endPortID"]) not in misconfigured.keys():
                            misconfigured["%s/%s" % (link["startPortID"], link["endPortID"])] = {"source": [], "target": []}
                        misconfigured["%s/%s" % (link["startPortID"], link["endPortID"])]["target"].append(vlan)
                for vlan in link["targetVlans"]:
                    if vlan not in link["sourceVlans"]:
                        print("VLAN %d configured on target port (%s), but not on source port (%s)" % (vlan, link["startPortID"], link["endPortID"]))
                        if "%s/%s" % (link["startPortID"], link["endPortID"]) not in misconfigured.keys():
                            misconfigured["%s/%s" % (link["startPortID"], link["endPortID"])] = {"source": [], "target": []}
                        misconfigured["%s/%s" % (link["startPortID"], link["endPortID"])]["source"].append(vlan)
            if "sourceNativeVlan" in link.keys() and "targetNativeVlan" in link.keys():
                if link["sourceNativeVlan"] != link["targetNativeVlan"]:
                    print("Native VLAN missmatch discovered between %s and %s" % (link["startPortID"], link["endPortID"]))
                    if "%s/%s" % (link["startPortID"], link["endPortID"]) not in misconfigured.keys():
                        misconfigured["%s/%s" % (link["startPortID"], link["endPortID"])]["nativeVlanMissmatch"] = True

        return misconfigured


    def link_cleanup(self):
        """
        This function cleans up orphaned links from topology
        :return:
        """
        device_ids = []
        for device in self.dict["nodes"]:
            device_ids.append(device["id"])

        for link in list(self.dict["links"]):
            source, target = (link["source"], link["target"])
            # print("\nLinkID: %s\nSource: %s %s\nTarget: %s %s" % (id, source, (source in device_ids), target, (target in device_ids)))
            if (source not in device_ids) or (target not in device_ids):
                self.dict["links"].remove(link)

    def device_cleanup(self):
        """
        This function cleans up orphaned devices from topology
        :return:
        """
        remove_device = None
        for node in list(self.dict["nodes"]):
            remove_device = True
            for link in self.dict["links"]:
                if link["source"] == node["id"] or link["target"] == node["id"]:
                    remove_device = False
            if remove_device:
                self.dict["nodes"].remove(node)

        pass


class PathTrace(Model):

    # TODO: Write PathTrace class
    def __init__(self, apic=None, filter=None, DEBUG=False, lowleveldebug=False):
        super().__init__(type="PathTrace", apic=apic, filter=filter, DEBUG=DEBUG, lowleveldebug=lowleveldebug)

    def runner(self):
        pass


class Modules(Model):
    # TODO: Write Modules class
    def __init__(self, apic=None, filter=None, passive=False, DEBUG=False, lowleveldebug=False):
        super(Modules, self).__init__(type="Module", apic=apic, filter=filter, DEBUG=DEBUG, lowleveldebug=lowleveldebug)

        self.map = None
        self.modules_file_path = pkg_resources.resource_filename("apicemapi", "Data/Modules.json")
        if not passive:
            self.update()

    def update(self):
        self.runner()
        self.dict = self.universal_cleanup()

    def runner(self):
        pass