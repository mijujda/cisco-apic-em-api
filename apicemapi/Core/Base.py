from __future__ import print_function
import json
import requests
import logging
import time
import re
import sys
import pkg_resources
from apicemapi.utils.utils import get_logger

class APIC_Core():

    def __init__(self, APIC_HOST=None, APIC_LOGIN=None, APIC_PASSWD=None, APIC_BASE=None, CREDENTIALS=None, DEBUG=False, VERIFY=False, cache_credentials=True):

        # Initialize Logging
        self.DEBUG = DEBUG
        self.logger = None
        self.logger = get_logger(name="APIC_Core", DEBUG=self.DEBUG)
        logging.basicConfig(level=logging.INFO)
        """
        if self.DEBUG:
            self.logger.setLevel(level=logging.DEBUG)
        else:
            self.logger.setLevel(logging.INFO)
        """
        # Disable debug logging for external libraries
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger("urllib3").setLevel(logging.WARNING)
        #
        # Global Vars
        self.APIC_HOST = APIC_HOST
        self.APIC_LOGIN = APIC_LOGIN
        self.APIC_PASSWD = APIC_PASSWD
        self.APIC_TICKET = None
        self.APIC_HEADERS = {"Content-type": "application/json"}
        self.APIC_BASE = APIC_BASE
        self.VERIFY = VERIFY
        self.CREDENTIALS = None
        if CREDENTIALS is None:
            self.CREDENTIALS = pkg_resources.resource_filename("apicemapi", "Data/credentials.json")
        else:
            self.CREDENTIALS = CREDENTIALS
        self.cache_credentials = cache_credentials
        #
        self.initialize()

    def load_credentials(self):
        """
        This method checks supplied data and sets required global variables
        """
        if (self.APIC_LOGIN is None) or (self.APIC_PASSWD is None) or (self.APIC_HOST is None):
            # Use credentials from credentials file
            self.logger.debug(msg="Loading credentials from file...")
            try:
                with open(self.CREDENTIALS, mode="r") as f:
                    credentials = json.load(f)
                    self.APIC_HOST = credentials["url"]
                    self.APIC_LOGIN = credentials["username"]
                    self.APIC_PASSWD = credentials["password"]
                    self.APIC_TICKET = credentials["token"]
                    self.APIC_HEADERS["X-Auth-Token"] = self.APIC_TICKET
                    self.logger.debug(msg="Successfully loaded credentials from '%s': Login: '%s' Password: '%s' Host: '%s' Ticket: %s" %
                                          (self.CREDENTIALS, self.APIC_LOGIN, self.APIC_PASSWD, self.APIC_HOST, self.APIC_TICKET))
            except Exception as e:
                self.logger.error(msg="Failed to load credentials from file, the exception states: '%s'" % e)
        else:
            self.logger.info(msg="Using supplied credentials.")
            self.write_credentials()
        self.APIC_BASE = "https://" + self.APIC_HOST + "/api/v1"

    def write_credentials(self):
        if self.cache_credentials:
            self.logger.debug(msg="Writing credentials to file '%s'" % self.CREDENTIALS)
            credentials = {"username": self.APIC_LOGIN, "password": self.APIC_PASSWD, "url": self.APIC_HOST,
                                       "token": self.APIC_TICKET}
            try:
                with open(self.CREDENTIALS, mode="w") as f:
                    json.dump(credentials, f)
                self.logger.debug(msg="Credentials successfully writen.")
            except Exception as e:
                self.logger.error(msg="Could not write credentials to file; the exceptions states: '%s'" % e)
        else:
            self.logger.info(msg="Credentials caching is disabled, set cache_credentials=True if you want to store credentials in file.")

    def initialize(self):
        self.load_credentials()
        if self.VERIFY == False:
            # Disable certificate warning
            try:
                requests.packages.urllib3.disable_warnings()
            except:
                self.logger.warning(msg="Failed to disable Certificate Warnings")

    def get_ticket(self):
        self.logger.debug(msg="Get Ticket: Starting Ticket renewal procedure...")
        # Remove old ticket from headers
        if "X-Auth-Token" in self.APIC_HEADERS.keys():
            del self.APIC_HEADERS["X-Auth-Token"]

        # Get New Ticket
        response = self.post(path="/ticket", data=json.dumps({"username": self.APIC_LOGIN, "password": self.APIC_PASSWD}))

        if response is not None:
            self.APIC_TICKET = response["serviceTicket"]
            self.APIC_HEADERS["X-Auth-Token"] = self.APIC_TICKET
            self.logger.info(msg="Get Ticket: Received new ticket: '%s'" % self.APIC_TICKET)
            self.write_credentials()
            return True
        else:
            self.logger.error(msg="Get Ticket: Failed to get ticket (for some reason). Exit()")
            return False

    def response_handler(self, response):
        try:
            status_code = response.status_code
            if status_code in range(200, 208):
                self.logger.debug(msg="Response Handler: Server returned Status Code: %d OK" % status_code)
                return response.json()["response"], status_code
            if status_code == 400:
                self.logger.error(msg="Response_Handler: Server returned Status Code: %d, Bad Request." % status_code)
                return None, status_code
            if status_code == 401:
                if response.json()["response"]["errorCode"] == "RBAC":
                    self.logger.error(msg="Response_Handler: Server returned Status Code: %d, Unauthorized. Generating new ticket..." % status_code)
                    return "retry", status_code
                if response.json()["response"]["errorCode"] == "INVALID_CREDENTIALS":
                    self.logger.error(msg="Response_Handler: Server returned Status Code: %d, Unauthorized. Please provide valid credentials" % status_code)
            if status_code == 404:
                if response.json()["response"]["errorCode"] == "Not found":
                    self.logger.error(msg="Response_Handler: Server returned Status Code: %d, Not Found. Message: '%s' Detail: '%s'" %
                                          (status_code, response.json()["response"]["message"], response.json()["response"]["detail"]))
                    return None, status_code
            if status_code in range(400,500):
                self.logger.error(msg="Response_Handler: Server returned Status Code: %d, cannot process response." % status_code)
                return None, status_code
            if status_code in range(500, 600):
                self.logger.error(msg="Response_Handler: Server returned Status Code: %d, cannot process response." % status_code)
                return None, status_code
        except Exception as e:
            self.logger.error(msg="Response_Handler: Failed to get response. Cannot process passed data. Exception states: '%s'" % e)
            return None, None

    def get(self, path, params=None):
        response = None
        if not isinstance(params, dict):

            try:
                response = requests.get(url=self.APIC_BASE + path, headers=self.APIC_HEADERS, verify=self.VERIFY)
            except Exception as e:
                self.logger.error(msg="GET: Failed to get response (Path: '%s'). The exception states: '%s'" % (path, e))
                return None

        else:

            try:
                response = requests.get(url=self.APIC_BASE + path, headers=self.APIC_HEADERS, verify=self.VERIFY, params=params)
            except Exception as e:
                self.logger.error(msg="GET: Failed to get response (Path: '%s'). The exception states: '%s'" % (path, e))
                return None

        self.logger.debug(msg="GET: Path: '%s' Headers: '%s' " % (path, self.APIC_HEADERS))
        # Ultra DEBUG Line, prints raw response.text:
        #self.logger.debug(msg="GET: Path: '%s' Headers: '%s' Received: '%s'" % (path, self.APIC_HEADERS, response.text))
        response, status_code = self.response_handler(response)
        if response is None:
            return None
        if response == "retry":
            # Generate new ticket
            got_ticket = self.get_ticket()
            # Call self again with new ticket
            if got_ticket:
                response = self.get(path=path, params=params)
                return response
        else:
            self.logger.debug(msg="GET: Status Code: %d Received response: %s" % (status_code, response))
            return response


    def post(self, path, data, params=None):
        if not isinstance(params, dict):
            try:
                response = requests.post(url=self.APIC_BASE + path, data=data, headers=self.APIC_HEADERS, verify=self.VERIFY)
                self.logger.debug(msg="POST: Path: '%s' Headers: '%s' Data: '%s'" % (path, self.APIC_HEADERS, data))
                # Ultra DEBUG Line, prints raw response.text:
                #self.logger.debug(msg="POST: Path: '%s' Headers: '%s' Data: '%s' Received: '%s'" % (path, self.APIC_HEADERS, data, response.text))
                response, status_code = self.response_handler(response)
                if response is None:
                    return None
                if response == "retry":
                    # Generate new ticket
                    got_ticket = self.get_ticket()
                    if got_ticket:
                        # Call self again with new ticket
                        response = self.post(path=path, data=data)
                        return response
                else:
                    self.logger.debug(msg="POST: Received response for Path: '%s' Data: '%s' Response: '%s'" % (path, data, response))
                    return response
            except Exception as e:
                self.logger.error(msg="POST: Failed to get response (Path: '%s'). The exception states: '%s'" % (path, e))
                return None
        else:
            # TODO: Handle Params
            self.logger.error(msg="POST: Handling Params not yet implemented!")
            return None

    def delete(self, path, params=None):
        if not params:
            try:
                response = requests.delete(url=self.APIC_BASE + path, headers=self.APIC_HEADERS, verify=self.VERIFY)
                self.logger.debug(msg="DELETE: Path: '{0}' Headers: '{1}''".format(path, self.APIC_HEADERS))
                response, status_code = self.response_handler(response)
                if response is None:
                    return None
                if response == "retry":
                    # Generate New Ticket
                    got_ticket = self.get_ticket()
                    if got_ticket:
                        # Call self again with new ticket
                        response = self.delete(path=path, params=params)
                        return response
                else:
                    # If everything goes well
                    self.logger.debug(msg="DELETE: Received response for Path: '{0}' Response: '{1}'".format(path, response))
                    return response

            except Exception as e:
                self.logger.error(msg="DELETE: Failed to get response (Path: '{0}'). The exception states: '{1}'".format(path, str(e)))
                return None
        else:
            #TODO: Handle Params
            self.logger.error(msg="DELETE: Handling Params not yet implemented!")

    def raw_get(self, path, params=None):
        # TODO: Handle ticket timeout
        if params is None:
            try:
                response = requests.get(url=self.APIC_BASE + path, headers=self.APIC_HEADERS, verify=self.VERIFY)
                self.logger.debug(msg="Raw GET: Path: '%s' Headers: '%s' " % (path, self.APIC_HEADERS))
                return response
            except Exception as e:
                self.logger.error(msg="Raw GET: Failed to get response (Path: '%s'). The exception states: '%s'" % (path, e))
                return None
        else:
            # TODO: Handle params
            self.logger.error(msg="POST: Handling Params not yet implemented!")
            return None


    def raw_post(self, path, data, params=None):
        # TODO: Handle ticket timeout
        if not isinstance(params, dict):
            try:
                response = requests.post(url=self.APIC_BASE + path, data=data, headers=self.APIC_HEADERS, verify=self.VERIFY)
                self.logger.debug(msg="Raw POST: Path: '%s' Headers: '%s' Data: '%s'" % (path, self.APIC_HEADERS, data))
                return response
            except Exception as e:
                self.logger.error(msg="POST: Failed to get response (Path: '%s'). The exception states: '%s'" % (path, e))
                return None
        else:
            # TODO: Handle params
            self.logger.error(msg="Raw POST: Handling Params not yet implemented!")
            return None


    def task_handler(self, taskId, timeout=2, max_retries=2):
        # Get Task ID
        progress = None
        result = None


        run_count = 0
        # Run loop until response contains Result ID or timeout
        while run_count < max_retries:
            response = self.get(path="/task/" + taskId)

            if response is not None:
                if response["isError"]:
                    self.logger.error(msg="Task Handler: Task '{0}' failed. Progress: '{1}' failureReason: '{2}'.".format(taskId, response["progress"], response["failureReason"]))
                    return None
                progress = str(response["progress"])
                self.logger.info(msg="Task Handler : Run:{0}: Progress is: '{1}'".format(run_count, progress))
                search_result = re.search(r"(?P<id>([A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}))", progress)
                if "Success" in progress and search_result:
                    result = search_result.group("id")
                    self.logger.debug(msg="Task Handler : Run:{0} : Found ResultID: '{1}'".format(run_count, result))
                    return result
                if "Success" in progress:
                    self.logger.info(msg="Task Handler : Run:{0} : Task '{1}' succeeded. Progress: '{2}'".format(run_count, taskId, progress))
                    return True
                else:
                    run_count += 1
                    time.sleep(timeout)
            else:
                self.logger.error(msg="Task Handler: Failed to get response for Task ID")
                return None
        self.logger.error(msg="Task Handler: Failed to get ResultID for TaskID: '{0}'. Reason: Timeout.".format(taskId))
        return None


    def file_exists(self, namespace, file_path):
        file_id = None
        file_name = file_path.split("/")[-1]
        namespaces = self.get(path="/file/namespace")
        if namespace in namespaces:
            self.logger.debug(msg="Namespace '%s' exists." % namespace)
            files = self.get(path="/file/namespace/%s" % namespace)
            for element in files:
                try:
                    if element["name"] == file_name:
                        file_id = element["id"]
                        self.logger.info(msg="File '%s' already exists." % file_name)
                        break
                except Exception as e:
                    pass
            if file_id is None:
                self.logger.info(msg="File '%s' does not exists." % file_name)
            return file_id
        else:
            self.logger.info(msg="Namespace '%s' does not exists." % namespace)
            return file_id

    def upload_file(self, namespace, file_path):
        file_name = file_path.split("/")[-1]
        file_id = self.file_exists(namespace=namespace, file_path=file_path)
        if file_id:
            self.logger.info(msg="File '{0}'in namespace '{1}' already exists with ID: '{2}'".format(file_name, namespace, file_id))
            return file_id

        if file_id is None:
            headers = dict(self.APIC_HEADERS)
            del headers["Content-type"]
            file = None
            try:
                file = open(file=file_path, mode="rb")
                self.logger.debug(msg="Upload file: File '%s' successfully opened." % file_path)
            except Exception as e:
                self.logger.error(msg="Upload file: Could not open file '%s'." % file_path)
                file = None
                return file_id
            try:
                print(file)
                response = requests.post(url=self.APIC_BASE + "/file/%s" % namespace, files={file_name: file}, headers=headers, verify=self.VERIFY)
                print(response.text)
                if response.status_code == 200:
                    file_id = response.json()["response"]["id"]
                    self.logger.info(msg="File {0} successfully uploaded with ID: '{1}'".format(file_name, file_id))
                return file_id
            except Exception as e:
                self.logger.info(msg="{0}: Exception: {1}".format(file_id, str(e)))
                return None


    def __str__(self):
        return "[APIC_CoreObject: %s]" % self.APIC_HOST


    def __repr__(self):
        return "[APIC_CoreObject: %s]" % self.APIC_HOST

if __name__ == '__main__':
    apic = APIC_Core(DEBUG=True)
    apic.get(path="/network-device/count")