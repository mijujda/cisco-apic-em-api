from apicemapi.Core.Models import Devices, Filter, OutputFilter
import json

devicefilter = Filter(required={"hostname": "Core1"}, exact_match=False)
interfacefilter = Filter(required={"portMode": "trunk"})


devices = Devices(filter=devicefilter, DEBUG=True, lowleveldebug=False)
devices.get_device_int(filter=interfacefilter)


int_no = 0
for id, device in list(devices.dict.items()):
    int_no += len(device["interfaces"])

print("Number of devices: ", len(devices.dict))
print("Number of interfaces: ", int_no)

devices.get_vlans()
devices.get_trunk_vlans(passive=True)


devices.write_data()

#print(json.dumps(OutputFilter(devices.dict, required=["hostname", "vlans"]).get(), indent=2))

