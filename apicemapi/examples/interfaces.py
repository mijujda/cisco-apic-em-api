from apicemapi.Core.Models import Filter, Interfaces, OutputFilter

filter = Filter(required={'portMode': "trunk"})
interfaces = Interfaces(filter=filter, DEBUG=True, lowleveldebug=False)
#interfaces.get_trunk_vlans()
filteredoutput = OutputFilter(interfaces.dict, required=["portName", "portMode", "vlanId"]).get()
print(len(filteredoutput), "Results")
print(filteredoutput)
