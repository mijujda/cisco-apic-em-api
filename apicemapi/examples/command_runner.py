import json

from Core.CommandRunner import CommandRunner

if __name__ == '__main__':

    # print json.load(open("CommandRunnerSampleData.json", mode="r"))
    data = CommandRunner(json.load(open("../Data/CommandRunnerSampleData.json", mode="r")), DEBUG=True).runner()

    print(json.dumps(data, indent=4))
    """
    parsers = []
    for element in data:
        for task in element["response"].keys():
            print(element["response"][task])
            parsers.append(Run_Parse(config=element["response"][task].split("\n")))

    for parser in parsers:
        print(parser.interface_list)
    """