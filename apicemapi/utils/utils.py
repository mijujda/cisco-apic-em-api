import logging
import sys
import os


def get_logger(name, DEBUG=False, logfile_path="../log.txt", handle=["stderr", "file"]):
    formatter = logging.Formatter("[%(asctime)s] : %(name)s - %(levelname)s - %(message)s")
    stdout_handler = logging.StreamHandler(sys.stdout)
    stderr_handler = logging.StreamHandler(sys.stderr)
    file_handler = logging.FileHandler(logfile_path)
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)

    handlers = []
    if "stderr" in handle:
        handlers.append(stderr_handler)
    if "stdout" in handle:
        handlers.append(stdout_handler)


    for handler in handlers:
        handler.setFormatter(formatter)
        if DEBUG:
            handler.setLevel(logging.DEBUG)
        else:
            handler.setLevel(logging.WARNING)

    root = logging.getLogger(name)
    root.setLevel(logging.DEBUG)
    root.propagate = 0
    root.addHandler(file_handler)
    for handler in handlers:
        root.addHandler(handler)

    return root


