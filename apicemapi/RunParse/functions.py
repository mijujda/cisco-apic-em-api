import re
import ciscoconfparse
from ciscoconfparse import CiscoConfParse

def interface_split(interface):
    # TODO: Make more reliable
    int_type, int_num = None, None
    if isinstance(interface, str):
        search_result = re.search(r"(?P<int_type>\D+)(?P<int_num>\d.*)", interface)
        try:
            int_type = search_result.group("int_type")
            int_num = search_result.group("int_num")
        except:
            pass
        finally:
            return int_type, int_num

    else:
        search_result = re.search(r"interface\s(?P<int_type>\D+)(?P<int_num>\d.*)", interface.text)
        try:
            int_type = search_result.group("int_type")
            int_num = search_result.group("int_num")
        except:
            pass
        finally:
            return int_type, int_num

def native_vlan(interface_object):
    # TODO: Make more reliable
    # Check if interface is trunk
    if len(interface_object.re_search_children("^\s+switchport mode trunk")) == 0: return None

    native_vlan = 1
    try:
        child = interface_object.re_search_children("^\s+switchport trunk native vlan")[0].text
        native_vlan = str(re.search(r"(?P<vlan_id>\d+)$", child).group("vlan_id"))
    except:
        pass
    finally:
        return native_vlan

def allowed_vlans(interface_object, short=True):
    # TODO: Make more reliable
    # Check if interface is trunk
    if len(interface_object.re_search_children("^\s+switchport mode trunk")) == 0:
        return None
    allowed_vlans_list = []
    raw_all_vl = []
    full_vlan_list = []
    children = interface_object.re_search_children("^\s+switchport trunk allowed vlan")
    if len(children) > 0:
        if "vlan all" in children[0].text:
            return "all"
        for child in children:
            child_vlans = re.search(r"(?P<vlan_group>\d.*)$", child.text).group("vlan_group").split(",")
            raw_all_vl = raw_all_vl + child_vlans
        for element in raw_all_vl:
            if "-" in element:
                ss_lst = element.split("-")
                start, stop = int(ss_lst[0]), int(ss_lst[1])
                full_vlan_list = full_vlan_list + (list(range(start, stop+1)))
            else:
                full_vlan_list.append(int(element))
        full_vlan_list.sort()
        if short:
            return vlan_range_shortener(full_vlan_list)
        else:
            return full_vlan_list
    else:
        return "all"


def vlan_range_shortener(full_vlan_list):
    # TODO: Make more reliable
    shortened_vlan_list = []
    # Initialize with first element
    temp_element = str(full_vlan_list[0])

    for i in range(1, len(full_vlan_list)-1):
        if (full_vlan_list[i-1] + 1 == full_vlan_list[i]) and (full_vlan_list[i] + 1 == full_vlan_list[i+1]):
            # Do nothing
            continue
        if full_vlan_list[i-1] + 1 == full_vlan_list[i] and (full_vlan_list[i] + 1 != full_vlan_list[i+1]):
            # Close temp element and write
            temp_element = temp_element + "-" + str(full_vlan_list[i])
            shortened_vlan_list.append(temp_element)
            temp_element = ""
            continue
        if full_vlan_list[i - 1] + 1 != full_vlan_list[i] and (full_vlan_list[i] + 1 == full_vlan_list[i + 1]):
            # Begin new temp element
            temp_element = str(full_vlan_list[i])
            continue
        if full_vlan_list[i - 1] + 1 != full_vlan_list[i] and (full_vlan_list[i] + 1 != full_vlan_list[i + 1]):
            if temp_element != "":
                shortened_vlan_list.append(temp_element)
                temp_element = ""
            shortened_vlan_list.append(str(full_vlan_list[i]))
            continue
    # Close with end element
    if full_vlan_list[-1] == full_vlan_list[-2] + 1:
        temp_element = temp_element + str(full_vlan_list[-1])
        shortened_vlan_list.append(temp_element)
        temp_element = ""
    else:
        shortened_vlan_list.append(str(full_vlan_list[-1]))
    return shortened_vlan_list


def vlan_brief_parser(data):
    vlans = {}

    pattern = re.compile(r"^(?P<id>\d+)\s+(?P<name>\S+)\s+(?P<status>\S+)\s+(?P<ports>(([A-Z][a-z]+\d+((/\d+)*))((,\s)|\s+|$))*)", re.MULTILINE)
    for m in re.finditer(pattern, data):
        try:
            vlan_id = int(m.group("id"))
            vlans[vlan_id] = {"name": m.group("name"), "status": m.group("status"), "ports": [x.strip() for x in m.group("ports").split(", ")]}
            if vlans[vlan_id]["ports"] == [""]:
                vlans[vlan_id]["ports"] = []
            for port in vlans[vlan_id]["ports"]:
                vlans[vlan_id]["ports"][vlans[vlan_id]["ports"].index(port)] = int_name_convert(port)
        except:
            pass
    return vlans

def int_name_convert(int_name):
    int_type, int_num = interface_split(int_name)
    short = ["Eth", "Se", "Fa", "Gi", "Te", "Po"]
    long = ["Ethernet", "Serial", "FastEthernet", "GigabitEthernet", "TenGigabitEthernet", "Port-channel"]
    new_int = ""
    if int_type in short:
        new_int = long[short.index(int_type)] + int_num
    elif int_type in long:
        new_int = short[long.index(int_type)] + int_num
    else:
        new_int = int_type + int_num
    return new_int
    # TODO: Write function to convert interface names

