from ciscoconfparse import CiscoConfParse
import logging
import json
from pprint import PrettyPrinter


class Run_Parse():
    def __init__(self, config):

        logging.basicConfig()
        self.logger = logging.getLogger(name="_IOS-Parser_")
        self.logger.setLevel(logging.WARNING)

        self.config = config
        self.numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        self.parser = CiscoConfParse(config=self.config)

        self.interface_objects_list = None
        self.get_interface_objects()
        self.interface_list = {}
        self.l2_interface_list = {}
        self.l3_interface_list = {}
        self.vlan_list = {}
        self.trunk_list = {}
        self.outfile = "output.json"

        self.get_interfaces()
        # self.get_l3_interfaces()
        # self.get_l2_interfaces()
        self.get_interface_properties()
        self.sync_interface()
        self.get_vlans()
        #self.get_trunks()


    def get_interface_objects(self, type=None):
        if type == None:
            self.interface_objects_list = self.parser.find_objects(linespec="^interface")
        if type == "Vlan" or type == "vlan":
            self.interface_objects_list = self.parser.find_objects(linespec="^interface Vlan")
        if type == "FastEthernet" or type == "fastethernet":
            self.interface_objects_list = self.parser.find_objects(linespec="^interface FastEthernet")
        if type == "GigabitEthernet" or type == "gigabitethernet":
            self.interface_objects_list = self.parser.find_objects(linespec="^interface GigabitEthernet")
        if not self.interface_objects_list:
            self.logger.error("_INTERFACES_Not valid interface type given")

    def get_interfaces(self):
        for interface in self.interface_objects_list:
            interface_name = self.crop(interface.text, ["interface"])
            self.interface_list[interface_name] = self.split_int(interface_name)

    def get_interface_properties(self):
        for interface in self.interface_objects_list:

            # L2 / L3 decision
            interface_name = self.crop(interface.text, ["interface"])
            try:
                child = interface.re_search_children("ip address")[0]
                self.interface_list[interface_name].update(
                    {"Type": "L3", "IP_address": {"IP": "", "Mask": ""}, "VRF": ""})
                self.logger.debug(msg="_IOS-Parser_:get_interface_properties: Interface %s type: L3" % interface_name)
                self.get_l3_interface_properties(interface_object=interface, interface_name=interface_name)
            except:
                self.interface_list[interface_name].update({"Type": "L2", "Sw_Mode": ""})
                self.logger.debug(msg="_IOS-Parser_:get_interface_properties: Interface %s type: L2" % interface_name)
                self.get_l2_interface_properties(interface_object=interface, interface_name=interface_name)

                # Description
                try:
                    child = interface.re_search_children("^ description")[0].text
                    self.interface_list[interface_name]["Description"] = self.crop(child, ["description"])
                    self.logger.debug(msg="_IOS-Parser_:Description for %s: %s" % (
                    interface_name, self.interface_list[interface_name]["Description"]))
                except Exception as e:
                    self.interface_list[interface_name]["Description"] = ""
                    self.logger.debug(msg="_IOS-Parser_:No description for %s, Exception: %s" % (interface_name, e))

                # Shutdown
                try:
                    child = interface.re_search_children("shutdown")[0].text
                    if child == " shutdown":
                        self.interface_list[interface_name]["Shutdown"] = "Yes"
                        self.logger.debug(msg="_IOS-Parser_:Shutdown : %s" % interface_name)
                    elif child == " no shutdown":
                        self.interface_list[interface_name]["Shutdown"] = "No"
                        self.logger.debug(msg="_IOS-Parser_:No shutdown : %s" % interface_name)
                except Exception as e:
                    self.interface_list[interface_name]["Shutdown"] = "No"
                    self.logger.debug(msg="_IOS-Parser_:No shutdown : %s, Exception: %s" % (interface_name, e))

    def get_l3_interface_properties(self, interface_object, interface_name):
        try:
            ip_address_line = self.crop(interface_object.re_search_children("ip address")[0].text, ["ip", "address"])
            ip_addr = ip_address_line.split(" ")
            ip = ip_addr[0]
            mask = ""
            if len(ip_addr) > 1:
                mask = ip_addr[1]
            self.interface_list[interface_name]["IP_address"]["IP"] = ip
            self.interface_list[interface_name]["IP_address"]["Mask"] = mask
            self.logger.debug(msg="_IOS-Parser_:get_l3_interface_properties: Interface %s IP: '%s', Mask: '%s'" % (
            interface_name, ip, mask))
        except Exception as e:
            self.logger.warning(
                msg="_IOS-Parser_:get_l3_interface_properties: No IP address statement for L3 interface %s" % interface_name)

    def get_l2_interface_properties(self, interface_object, interface_name):
        type = None
        # Trunk/Access
        try:
            trunk_line = interface_object.re_search_children("switchport mode trunk")[0].text
            self.interface_list[interface_name].update({"Sw_Mode": "Trunk", "Trunk": {"Native_VLAN": "", "Allowed_VLANs": ""}})
            self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties: Interface %s: mode TRUNK" % interface_name)
            type = "Trunk"
        except:
            self.interface_list[interface_name].update({"Sw_Mode": "Access", "Access": {"VLAN": ""}})
            self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties: Interface %s: mode ACCESS" % interface_name)
            type = "Access"

        if type == "Trunk":
            # Native VLAN
            try:
                native_vlan = self.crop(interface_object.re_search_children("switchport trunk native vlan")[0].text,
                                        ["switchport", "trunk", "native", "vlan"])
                self.interface_list[interface_name]["Trunk"]["Native_VLAN"] = int(native_vlan)
                self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties: Interface %s native VLAN: %d" % (
                interface_name, native_vlan))
            except Exception as e:
                native_vlan = 1
                self.interface_list[interface_name]["Trunk"]["Native_VLAN"] = int(native_vlan)

                self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties: Interface %s native VLAN: %d" % (
                    interface_name, native_vlan))
                self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties:Native_VLAN: Exception %s" % e)
            # Allowed VLANs
            try:
                allowed_vlans = self.crop(interface_object.re_search_children("switchport trunk allowed vlan")[0].text,
                                          ["switchport", "trunk", "allowed", "vlan"])
                self.interface_list[interface_name]["Trunk"]["Allowed_VLANs"] = allowed_vlans
                self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties: Interface %s allowed VLAN: %s" % (
                interface_name, allowed_vlans))
            except Exception as e:
                self.interface_list[interface_name]["Trunk"]["Allowed_VLANs"] = "1-4096"
                self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties: Interface %s allowed VLAN: %s" % (
                    interface_name, "1-4096"))
                self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties:Allowed_VLANs: Exception %s" % e)
        elif type == "Access":
            try:
                access_vlan = self.crop(interface_object.re_search_children("switchport access vlan")[0].text)
                self.interface_list[interface_name]["Access"]["VLAN"] = int(access_vlan)
                self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties: Interface %s access VLAN: %d" % (interface_name, access_vlan))
            except Exception as e:
                self.interface_list[interface_name]["Access"]["VLAN"] = 1
                self.logger.debug(msg="_IOS-Parser_:get_l2_interface_properties: Interface %s access VLAN: %d" % (interface_name, 1))

    def get_vlans(self):
        vlans_objects = self.parser.find_objects(linespec="^vlan")
        for vlan in vlans_objects:
            vlan_id = int(self.crop(vlan.text, ["vlan"]))
            self.vlan_list[vlan_id] = {"Name": "", "Trunks": [], "Access": []}
            children = vlan.re_search_children("^ name")
            try:
                self.vlan_list[vlan_id]["Name"] = self.crop(children[0].text, ["name"])
                self.logger.debug(
                    msg="_IOS-Parser_:VLAN %s: name: %s" % (vlan_id, self.crop(children[0].text, ["name"])))
            except:
                self.vlan_list[vlan_id]["Name"] = ""
                self.logger.debug(msg="_IOS-Parser_:VLAN %s: name: %s" % (vlan_id, "NONE"))

            for interface in self.interface_list.keys():
                if self.interface_list[interface]["Type"] == "L3": continue
                elif self.interface_list[interface]["Type"] == "L2":
                    if self.interface_list[interface]["Sw_Mode"] == "Access":
                        if self.interface_list[interface]["Access"]["VLAN"] == vlan_id:
                            self.vlan_list[vlan_id]["Access"].append(interface)
                    elif self.interface_list[interface]["Sw_Mode"] == "Trunk":
                        if self.check_vlan_range(ran_str=self.interface_list[interface]["Trunk"]["Allowed_VLANs"], vlan=vlan_id):
                            self.vlan_list[vlan_id]["Trunks"].append(interface)

    def check_vlan_range(self, ran_str, vlan):
        range_list = ran_str.split(",")
        final_range = []
        for item in range_list:
            if "-" in item:
                start, stop = item.split("-")
                final_range = final_range + range(int(start), int(stop) + 1)

            else:
                final_range.append(int(item))
        final_range.sort()
        if int(vlan) in final_range:
            return True
        else:
            return False

    def sync_interface(self):
        # L2-3 interfaces
        for interface in self.interface_list.keys():
            if self.interface_list[interface]["Type"] == "L2":
                self.l2_interface_list[interface] = self.interface_list[interface]
            elif self.interface_list[interface]["Type"] == "L3":
                self.l3_interface_list[interface] = self.interface_list[interface]

    def dump_json(self):
        all = {"Interfaces": self.interface_list, "VLANs": self.vlan_list }
        with open(self.outfile, mode="w") as f:
            json.dump(all, fp=f, indent=4)


    def crop(self, line, cut):
        # Cut is string that should be removed from line
        array = line.split(" ")
        cut.append(" ")
        # if self.DEBUG: print "CROP FUNCTION for line %s: %s" % (line, array)
        new_line = ""
        for word in array:
            if (word not in cut):
                new_line = new_line + word + " "
        new_line = new_line[:-1]
        # if self.DEBUG: print "Cropped line is: %s" %new_line
        return new_line.strip()

    def split_int(self, interface):
        interface_dict = {"Int_Type": "", "Number": ""}
        for i in range(0, len(interface)):
            help = interface[i]
            if interface[i] not in self.numbers:
                continue
            else:
                interface_dict["Int_Type"] = interface[0:i]
                interface_dict["Number"] = "%s" % interface[i:]
                break
        # if self.DEBUG: print interface_dict
        return interface_dict
