from .functions import *
from ciscoconfparse import CiscoConfParse

class L2_Trunk():
    # TODO: Make more reliable, add logging
    # TODO: Handle both string and list configs

    def __init__(self, config):
        self.config = config
        self.parser = CiscoConfParse(config=self.config)
        self.interface_object = self.parser.find_objects(linespec="^interface")[0]
        self.dict = {}

        self.runner()

    def runner(self):
        int_type, int_num = interface_split(self.interface_object)
        nat_vlan = native_vlan(self.interface_object)
        all_vl = allowed_vlans(self.interface_object, short=False)
        self.dict = {
            "intType": int_type,
            "intNum": int_num,
            "nativeVlan": nat_vlan,
            "allowedVlans": all_vl
             }

class VlanBrief():

    def __init__(self, config):
        self.config = config
        self.dict = {}
        self.runner()

    def runner(self):
        self.dict = vlan_brief_parser(self.config)
