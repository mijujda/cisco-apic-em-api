import sys
import os
import json
from apicemapi.utils.utils import get_logger

class ReadWrite:
    def __init__(self, DEBUG=False):
        self.logger = get_logger(name="ReadWrite", DEBUG=DEBUG)
        self.base_path = os.path.join(os.path.dirname(__file__).split("apicemapi")[0], "apicemapi")
        self.logger.debug(msg=f"Base Path is {self.base_path}")

    def path_check(self, path, create_missing=False):
        # Check if path exists:
        if os.path.exists(path):
            path = os.path.abspath(path)
            self.logger.debug(msg=f"Absolute path {path} exists.")
        else:
            print(path)
            path = os.path.abspath(os.path.join(self.base_path, path))
            self.logger.debug(msg=f"Relative path {path} exists.")
        return path
        

    def read_json(self, filepath):
        # TODO: Implement read_json function
        pass

    def read_csv(self, filepath):
        filepath = self.path_check(filepath)



if __name__ == '__main__':
    rw = ReadWrite(DEBUG=True)
    rw.path_check(r"examples")
